"""Who, me? I didn't say that! I'm not on trial here.

Acts on your behalf to remove messages from a slack workspace using your API
token.  This token is a string that can either be passed by an environment
variable (SLACK_API_TOKEN) or by file at ~/.ssh/slack_token. You can create a
token at https://api.slack.com/custom-integrations/legacy-tokens

Usage:
  idst.py [--dry-run] [--query=<text>] [--verbose] [--max=<max>]
  idst.py (-h | --help)
  idst.py --version

Options:
  -h --help       Show this screen.
  --version       Show version.
  --query=<text>  Specify the text used for searching messages. [default: IDST]
  --dry-run       Print the messages to be removed, but doesn't delete them.
  --max=<max>     If specified, the maximum number of messages that will be 
                  deleted in a single run of the script.
"""

import docopt
import os
import slack
import time
import logging as log


class IDidntSayThat:

    """Acts on your behalf to remove messages from a slack workspace."""

    # If reading the slack token from a file, the location to search.
    token_file = "{}/.ssh/slack_token".format(os.environ["HOME"])

    def __init__(self, docopts):
        """Initialize the utility from the arguments."""
        self.docopts = docopts

        # Set the log level immediately.
        verbose = docopts["--verbose"]
        if verbose:
            log.getLogger().setLevel(log.INFO)
        log.info("Docopt arguments: {}".format(docopts))

        # Lazy initialization.
        self._client = None
        self._user = None

    def client(self):
        """Return an initialized client for interacting with slack."""
        if self._client is None:
            # Get the token from the environment or from a file.
            token = os.environ.get("SLACK_API_TOKEN", None)
            if token is not None:
                log.info("Using API token from environment")
            else:
                log.info("Using API token from file {}".format(self.token_file))
                with open(self.token_file) as token_input:
                    token = token_input.readline().strip()
            self._client = slack.WebClient(token=token)

        return self._client

    def user(self):
        """Return the user that the client and token represent."""
        if self._user is None:
            whoami = self.client().auth_test()
            self._user = whoami.data["user"]
            log.info("Connected as {}: {}".format(self._user, whoami))
        return self._user

    def search_messages(self, query, count):
        """Generator for paging over all of the available messages."""
        # TODO: Currently not used.
        # TODO: Add back pagination / timeout / retry.
        while True:
            search_results = self.client().search_messages(
                query=query, count=count, sort="timestamp", sort_dir="asc"
            )
            # If no results, stop looking.
            if search_results.data["messages"]["total"] == 0:
                break
            for msg in search_results.data["messages"]["matches"]:
                yield msg


# Get the command line arguments
arguments = docopt.docopt(__doc__, version="IDST 0.0.1-SNAPSHOT")

dry_run = arguments["--dry-run"]
max_msg = arguments["--max"]
if max_msg is None:
    max_msg = -1

idst = IDidntSayThat(arguments)
client = idst.client()
username = idst.user()

query = arguments.get("--query", "IDST")
if username is not None:
    query = "{} from:{}".format(query, username)

res_pages = client.search_messages(query=query, count=100)
num_pages = res_pages.data["messages"]["paging"]["pages"]
for i in range(num_pages):
    # TODO: If we have deleted ALL of the messages on page 1, wouldn't all
    # of the results on page 2 be moved up?  And then missed?
    # TODO: Start on the last pages?
    # TODO: sort="timestamp" ?  What does "score" mean?
    # TODO: add the last deleted timestamp to the query?
    # (only older/newer messages)?
    res_search_messages = client.search_messages(query=query, count=100, page=i)
    for msg in res_search_messages.data["messages"]["matches"]:
        if msg["username"] == username:
            if dry_run:
                print("%s %s: %s" % (msg["ts"], msg["username"], msg["text"]))
            else:
                res_chat_delete = client.chat_delete(
                    channel=msg["channel"]["id"], ts=msg["ts"]
                )
                if res_chat_delete.status_code == 200:
                    print("%s %s: %s" % (msg["ts"], msg["username"], msg["text"]))
                    if max_msg > 0:
                        max_msg -= 1
                        if max_msg <= 0:
                            log.info("---- SWITCHING TO DRY RUN ------")
                            dry_run = True
                # chat_delete is rate limited at 50 requests per minute so we sleep a bit
                time.sleep(1.2)
