# slack-idst

Who, me?  I didn't say that!  I'm not on trial here.


## Python

Python 3 obviously! Code auto-formatted with `black`.

```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt

export SLACK_API_TOKEN=....
export SLACK_USERNAME=....
python idst.py
```
